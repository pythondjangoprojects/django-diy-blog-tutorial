from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from datetime import date

# Create your models here.
class BlogAuthor(models.Model):
    """Model represnting a Blog author called bloggers"""
    bio = models.TextField(max_length=400, help_text = 'Enter Bio details')
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    class Meta:
        ordering = ["user","bio"]

    def get_absolute_url(self):
        """ Returns the url to access a particular blog-author instance."""
        return reverse('blogs-by-author', args=[str(self.id)])
    
    def __str__(self):
        """String for represnting the Model Blog."""
        return self.user.username

class Blog(models.Model):
    """Model representing a Blog"""
    name = models.CharField(max_length=200, help_text='Enter a blog name(e.g. Science Fiction)')
    description = models.TextField(max_length=2000, help_text='Enter a blog description(e.g. Science Fiction)')
    author = models.CharField(max_length=200, help_text='Enter a blog description(e.g. Science Fiction)')
    author = models.ForeignKey(BlogAuthor, on_delete=models.SET_NULL, null=True)
    post_date = models.DateField(default=date.today)

    class Meta:
        ordering = ["-post_date"]

    def __str__(self):
        """String for representing the Model object."""
        return self.name

    def get_absolute_url(self):
        return reverse('blog-detail', args=[str(self.id)])

class BlogComment(models.Model):
    """Model represnting the blof comment"""
    description = models.TextField(max_length = 4000, help_text='Enter the blog comment')
    post_date = models.DateField(default=date.today)
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    Blog = models.ForeignKey(Blog, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        """String for representing the Model object."""
        len_title = 75
        if len(self.description)>len_title:
            titlestring=self.description[:len_title] + '...'
        else:
            titlestring=self.description
        return titlestring